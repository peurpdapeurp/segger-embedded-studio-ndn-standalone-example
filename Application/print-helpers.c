
#include "print-helpers.h"

void print_hex(const char *msg, uint8_t *buf, int buf_len) {
  printf("%s\n", msg);
  int i;
  for (i = 0; i < buf_len; i++) {
    if (i > 0)
      printf(":");
    printf("%02X", buf[i]);
  }
  printf("\n");
}