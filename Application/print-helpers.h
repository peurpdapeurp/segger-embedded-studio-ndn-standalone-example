
#ifndef PRINT_HELPERS_H
#define PRINT_HELPERS_H

#include <stdint.h>
#include <stdio.h>

void print_hex(const char *msg, uint8_t *buf, int buf_len);

#endif // PRINT_HELPERS_H