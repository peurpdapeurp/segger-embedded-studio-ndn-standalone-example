
#include "ndn-nrf-ble-face.h"
#include "../encode/data.h"
#include <stdio.h>

static ndn_nrf_ble_face_t nrf_ble_face;

ndn_nrf_ble_face_t*
ndn_nrf_ble_face_get_instance()
{
  return &nrf_ble_face;
}

/************************************************************/
/*  Adaptation Helper Functions                             */
/************************************************************/

// TODO fill these in

/************************************************************/
/*  Inherit Face Interfaces                                 */
/************************************************************/

int
ndn_nrf_ble_face_up(struct ndn_face_intf* self)
{
  self->state = NDN_FACE_STATE_UP;
  return 0;
}

int
ndn_nrf_ble_face_send(struct ndn_face_intf* self, const ndn_name_t* name,
                         const uint8_t* packet, uint32_t size)
{
  printf("ndn_nrf_ble_face_send got called.\n");
}


int
ndn_nrf_ble_face_down(struct ndn_face_intf* self)
{
  self->state = NDN_FACE_STATE_DOWN;
  return 0;
}

void
ndn_nrf_ble_face_destroy(struct ndn_face_intf* self)
{
  self->state = NDN_FACE_STATE_DESTROYED;
  return;
}

ndn_nrf_ble_face_t*
ndn_nrf_ble_face_construct(uint16_t face_id)
{
  nrf_ble_face.intf.up = ndn_nrf_ble_face_up;
  nrf_ble_face.intf.send = ndn_nrf_ble_face_send;
  nrf_ble_face.intf.down = ndn_nrf_ble_face_down;
  nrf_ble_face.intf.destroy = ndn_nrf_ble_face_destroy;
  nrf_ble_face.intf.face_id = face_id;
  nrf_ble_face.intf.state = NDN_FACE_STATE_DESTROYED;
  nrf_ble_face.intf.type = NDN_FACE_TYPE_NET;

  return &nrf_ble_face;
}