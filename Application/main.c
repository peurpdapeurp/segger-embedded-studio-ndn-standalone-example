/**
 * Copyright (c) 2014 - 2018, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 *
 * @defgroup blinky_example_main main.c
 * @{
 * @ingroup blinky_example
 * @brief Blinky Example Application main file.
 *
 * This file contains the source code for a sample application to blink LEDs.
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include "nrf_delay.h"
#include "boards.h"

// includes for ndn standalone library
#include "ndn_standalone/face/direct-face.h"
#include "ndn_standalone/face/ndn-nrf-ble-face.h"
#include "ndn_standalone/encode/interest.h"
#include "ndn_standalone/encode/encoder.h"
#include "ndn_standalone/forwarder/forwarder.h"

// defines for ndn standalone library

ndn_direct_face_t *m_face;
uint16_t m_face_id_direct = 2;
uint16_t m_face_id_ble = 3;

ndn_nrf_ble_face_t *m_ndn_nrf_ble_face;

int m_on_data_callback (const uint8_t* data, uint32_t data_size) {
  printf("Data callback was triggered.");
  return 0;
}

int m_interest_timeout_callback (const uint8_t* interest, uint32_t interest_size) {
  printf("Interest callback was triggered.");
  return 0;
}

/**
 * @brief Function for application main entry.
 */
int main(void)
{
    /* Configure board. */
    bsp_board_init(BSP_INIT_LEDS);

    // initialize the forwarder
    ndn_forwarder_init();

    // create an interest name
    ndn_name_t dummy_interest_name;
    char dummy_interest_name_string[] = "/dummy/interest";
    ndn_name_from_string(&dummy_interest_name, dummy_interest_name_string, strlen(dummy_interest_name_string));

    // create a ble face, which will receive the interest we send through the direct face
    m_ndn_nrf_ble_face = ndn_nrf_ble_face_construct(m_face_id_ble);
    m_ndn_nrf_ble_face->intf.state = NDN_FACE_STATE_UP;

    // insert the ble face into the forwarding information base with the dummy interest's name, so that the
    // direct face's interest gets routed to this ble face
    int ret;
    if ((ret = ndn_forwarder_fib_insert(&dummy_interest_name, &m_ndn_nrf_ble_face->intf, 0)) != 0) {
      printf("Problem inserting fib entry, error code %d\n", ret);
    }

    printf("Finished creating ble face and inserting it into FIB.\n");

    // create a direct face, which we will use to send the interest
    m_face = ndn_direct_face_construct(m_face_id_direct);

    printf("Finished constructing the direct face.\n");

    // create an interest, set its name to the dummy name
    ndn_interest_t dummy_interest;
    ndn_interest_from_name(&dummy_interest, &dummy_interest_name);

    printf("Finished initializing the dummy interest.\n");

    // initialize the encoder
    ndn_encoder_t interest_encoder;
    uint32_t encoded_interest_max_size = 500;
    uint8_t encoded_interest_buf[encoded_interest_max_size];
    encoder_init(&interest_encoder, encoded_interest_buf, encoded_interest_max_size);

    printf("Finished initializing the interest encoder.\n");

    // tlv encode the interest
    ndn_interest_tlv_encode(&interest_encoder, &dummy_interest);

    printf("Finished encoding the ndn interest.\n");

    // express the encoded interest
    ndn_direct_face_express_interest(
      &dummy_interest_name,
      interest_encoder.output_value,
      interest_encoder.offset,
      m_on_data_callback,
      m_interest_timeout_callback);

    printf("Finished expressing the interest to the ndn direct face.\n");

    /* Toggle LEDs. */
    while (true)
    {
        for (int i = 0; i < LEDS_NUMBER; i++)
        {
            bsp_board_led_invert(i);
            nrf_delay_ms(500);
        }
    }
}

/**
 *@}
 **/
